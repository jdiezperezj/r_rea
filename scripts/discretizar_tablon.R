# Discretizing varibles for both Auto and Hogar panels.
# Check Variables beforehand
# Others

set.seed(12345)
sink(tempfile())

# List required librariesdplyr
.packages <- c("glue", "readr","optparse", "lezor", "plyr", "dplyr", 
               "magrittr", "tibble", "stringr", "tidyr")

# Install CRAN packages (if not already installed)
.inst <- .packages %in% installed.packages()
if (length(.packages[!.inst]) > 0) install.packages(.packages[!.inst])
# Load packages into session 
lapply(.packages, require, character.only = TRUE)
sink()


# Custumised cut function.
# If highest cut == vector maximo -> highest group will be empty.
calculate_integer_cuts <- function(vector, breaks, labels=NULL){
  maxv <- max(vector, na.rm = TRUE)
  minv <- min(vector, na.rm = TRUE)
  maxb <- max(breaks, na.rm = TRUE)
  minb <- min(breaks, na.rm = TRUE)
  minimo <- min(c(minb, minv), na.rm = TRUE)
  maximo <- max(c(maxb, maxv), na.rm = TRUE)
  minimo <- ifelse(minimo == minb, minimo - 1, minimo) # Force lower limit to be lower than lowest break
  if(minimo < minv){
    warning('Lower limit group will contain only lowest value.\n')
    }
  maximo <- ifelse(maximo == maxb, maximo + 1, maximo) # Force upper limit to be higher than highest break
  if(maximo > maxv){
    warning('Upper limit group will be empty.\n')
  }
  if(is.null(labels)){
    nlabels = c(paste('<=', breaks), paste('>', breaks[length(breaks)]))
  }else{
    nlabels = labels
  }
  breaks = c(minimo, breaks, maximo)
  cuts <- cut(vector, breaks, labels=nlabels, include.lowest = TRUE) %>% as.character()
  return(cuts)
}

calculate_cuts_or_relabel <- function(vector, breaks, labels=NULL, option='cut') {
  switch(option,
    cut = {
      calculate_integer_cuts(vector, breaks, labels)
    },
    relabel = {
      mapvalues(vector %>% as.character(), from = breaks %>% as.character(), to = labels)
    },
    stop("Invalid option: availables are 'cuts' and 'relable'.\n")
  )
}


relabel <- function(original_labels, n_suffixes=c('98_','99_'), n=0, padding=2){
  # # etiqueta con leading 0s salvo que comience con '98_' o '99_', podr�a simplicarse en paso anterior.
  # labels <- apply(d %>% select(Label, iLabel), 1,
  #       function(x) ifelse(!(startsWith(x[[1]],'99_') & startsWith(x[[1]],'98_')), 
  #                          paste(str_pad(x[[2]], m_char, pad=0), x[[1]], sep='_'), 
  #                          x[[1]])) %>% as.character()
  m_char <- (length(original_labels) - length(n_suffixes)) %>% 
    as.character() %>% 
    nchar() # dimensions' order of magnitude.
  m_char <- ifelse(m_char < padding, padding, m_char)
  ni <- n
  labels <- lapply(original_labels, 
                   function(x){
                     if(!any(lapply(n_suffixes, function(y) startsWith(x, y)))){
                       # if(!startsWith(x, '98_') & !startsWith(x, '99_')){
                       ni <<- ni + 1; 
                       nx <- paste(str_pad(ni, m_char, pad=0), x, sep='_')
                       return(nx)
                     }else{
                       return(x)
                     }
                   }
  ) %>% unlist()
  return(labels)
}


discretize_variable <- function(x, df){
  v <- x$Variable
  d <- x$data
  if(v %in% colnames(df)){
    data <- df[[v]] %>% as.vector()
    tipo <- d$Tipo[1]
    # labels <- relabel(d$Label %>% as.character())
    labels <- d$Label %>% as.character()
    if(tipo != 'Categorica'){
      cat('Numerica ...')
      breaks <- d$Valor %>% as.numeric() %>% unique() %>% sort()
      res <- calculate_cuts_or_relabel(data, breaks, labels=labels, option='cut')
    }else{
      cat('Categorica ...')
      breaks <- d$Valor %>% as.character()
      res <- calculate_cuts_or_relabel(data, breaks, labels=labels, option='relabel')
      res[!(res %in% labels)] <- "98_Otros"
    }
    return(res)
  }else{
    NA
  }
}


read_tablon <- function(tablename, schema='WORK'){
  f_ini_lezor()
  f_conecta_con_lezo()
  
  tablon <- lezor::f_query_a_df(select = '*', from = glue('{schema}.Mod_Abandono_{tablename}')) %>% as_tibble()
  
  f_desconectar_de_lezo()
  
  return(tablon)
}


write_tablon <- function(df, name){
  f_ini_lezor()
  f_conecta_con_lezo()
  
  lezor::f_escribe_en_lezo(df = df,
                           nombre_tabla = glue("WORK.Mod_Abandono_{name}"),
                           sobreescribir = F,
                           ruta_temp_files = "\\\\wpg001cli\\lezor_shared\\temp\\")
  
  f_desconectar_de_lezo()
}


main <- function(opt){
  # -----------------
  # Get categorias
  # -----------------
  
  #categorias <- read_tsv(opt$input)
  f<-'C:\\Users\\JDD\\Documents\\reale\\server1\\TABLON\\DISCRETIZACION\\discretizacion.tsv'
  categorias <- read_tsv(f)
  # -----------------
  # HOGAR
  # -----------------
  
  c_hogar <- categorias %>% filter(Ramo=='hogar') %>% group_by(Variable) %>% nest()
  
  hogar <- read_tablon(opt$schema, opt$hogar)
  
  d_hogar <- apply(c_hogar, 1, discretize_variable, df=hogar %>% select(-Cliente_Codigo, -Poliza_Codigo, -A�oMes_Control, -Anula_Valor))
  names(d_hogar) <- c_hogar$Variable %>% as.character()
  d_hogar %<>% as_tibble()
  d_hogar <- cbind(d_hogar, hogar[,c('Cliente_Codigo', 'Poliza_Codigo','A�oMes_Control','Anula_Valor')])
  
  d_hogar[is.na(d_hogar)] <- '99_No_Disponible' #opt$nas
  
  saveRDS(d_hogar,'tablon_hogar_disc.rds')
  
  write_tablon(d_auto, 'TablonHogarDisc')
  
  
  # ---------------
  # AUTO
  # ---------------
  
  c_auto <- categorias %>% filter(Ramo=='auto') %>% group_by(Variable) %>% nest()
  
  auto <- read_tablon(opt$schema, opt$auto)
  
  d_auto <- apply(c_auto, 1, discretize_variable, df=auto %>% select(-Cliente_Codigo, -Poliza_Codigo, -A�oMes_Control, -Anula_Valor))
  names(d_auto) <- c_auto$Variable %>% as.character()
  d_auto %<>% as_tibble()
  d_auto <- cbind(d_auto, auto[,c('Cliente_Codigo', 'Poliza_Codigo','A�oMes_Control','Anula_Valor')])
  
  d_auto[is.na(d_auto)] <- '99_No_Disponible' #opt$nas
  
  saveRDS(d_auto, 'tablon_auto_disc.rds')
  
  write_tablon(d_auto, 'TablonAutoDisc')
  
}


option_list = list(
  make_option(c("-a", "--auto"), type = "character",
              default = "TablonAuto",
              help = "File with variables to include",
              metavar = "character"),
  make_option(c("-h", "--hogar"), type = "character",
              default = "TablonHogar",
              help = "File with variables to include",
              metavar = "character"),
  make_option(c("-i", "--input"), type = "character",
              default = "\\\\wpg001cli\\INMARK_Modelos_abandono\\TABLON\\DISCRETIZACION\\discretizacion.tsv",
              help = "File with variables to include",
              metavar = "character"),
  make_option(c("-s", "--schema"), type = "character",
              default = "WORK",
              help = "File with variables to include",
              metavar = "character"),
  make_option(c("-n", "--nas"), type = "character",
              default = "99_No_Disponible",
              help = "Replacement for NAs.",
              metavar = "character"),
  make_option(c("-r", "--rds"), type = "character",
              default = "",
              help = "Replacement for NAs.",
              metavar = "character")

)

opt_parser <- OptionParser(option_list = option_list);
opt <- parse_args(opt_parser);

main(opt)