# Discretizing varibles for both Auto and Hogar panels.
# Check Variables beforehand
# Others

set.seed(12345)
sink(tempfile())

# List required librariesdplyr
.packages <- c("glue", "stringr", "lubridate", "readr", "optparse", 
               "plyr", "dplyr", "magrittr", "tibble", "stringr", "tidyr")

# Install CRAN packages (if not already installed)
.inst <- .packages %in% installed.packages()
if (length(.packages[!.inst]) > 0) install.packages(.packages[!.inst])
# Load packages into session 
lapply(.packages, require, character.only = TRUE)
sink()


# Custumised cut function.
# If highest cut == vector maximo -> highest group will be empty.
calculate_integer_cuts <- function(vector, breaks, labels=NULL){
  maxv <- max(vector, na.rm = TRUE)
  minv <- min(vector, na.rm = TRUE)
  maxb <- max(breaks, na.rm = TRUE)
  minb <- min(breaks, na.rm = TRUE)
  minimo <- min(c(minb, minv), na.rm = TRUE)
  maximo <- max(c(maxb, maxv), na.rm = TRUE)
  minimo <- ifelse(minimo == minb, minimo - 1, minimo) # Force lower limit to be lower than lowest break
  if(minimo < minv){
    warning('Lower limit group will contain only lowest value.\n')
  }
  maximo <- ifelse(maximo == maxb, maximo + 1, maximo) # Force upper limit to be higher than highest break
  if(maximo > maxv){
    warning('Upper limit group will be empty.\n')
  }
  if(is.null(labels)){
    nlabels = c(paste('<=', breaks), paste('>', breaks[length(breaks)]))
  }else{
    nlabels = labels
  }
  breaks = c(minimo, breaks, maximo)
  cuts <- cut(vector, breaks, labels=nlabels, include.lowest = TRUE) %>% as.character()
  return(cuts)
}


calculate_cuts_or_relabel <- function(vector, breaks, labels=NULL, option='cut') {
  switch(option,
         cut = {
           calculate_integer_cuts(vector, breaks, labels)
         },
         relabel = {
           mapvalues(vector, from = breaks, to = labels)
         },
         stop("Invalid option: availables are 'cuts' and 'relable'.\n")
  )
}


relabel <- function(original_labels, n_suffixes=c('98_','99_'), n=0, padding=2){
  m_char <- (length(original_labels) - length(n_suffixes)) %>% 
    as.character() %>% 
    nchar() # dimensions' order of magnitude.
  m_char <- ifelse(m_char < padding, padding, m_char)
  ni <- n
  labels <- lapply(original_labels, 
                   function(x){
                     if(!any(lapply(n_suffixes, function(y) startsWith(x, y)))){
                       ni <<- ni + 1; 
                       nx <- paste(str_pad(ni, m_char, pad=0), x, sep='_')
                       return(nx)
                     }else{
                       return(x)
                     }
                   }
  ) %>% unlist()
  return(labels)
}


discretize_variable <- function(x, df){
  v <- x$Variable
  d <- x$data
  if(v %in% colnames(df)){
    data <- df[[v]] %>% as.vector()
    tipo <- d$Tipo[1]
    labels <- d$Label %>% as.character()
    if(tipo != 'Categorica'){
      #cat(glue('Numerica ...{x$Variable}\n'))
      cat(v)
      breaks <- d$Valor %>% as.numeric() %>% unique() %>% sort()
      res <- calculate_cuts_or_relabel(data, breaks, labels=labels, option='cut')
    }else{
      #cat(glue('Categorica ...{x$Variable}\n'))
      breaks <- d$Valor %>% as.character()
      res <- calculate_cuts_or_relabel(data, breaks, labels=labels, option='relabel')
      res[!(res %in% labels)] <- "98_Otros"
    }
    return(res)
  }else{
    NA
  }
}


read_tablon <- function(tablename, schema='WORK'){
  f_ini_lezor()
  f_conecta_con_lezo()
  
  tablon <- lezor::f_query_a_df(select = '*', 
                                from = glue('{schema}.Mod_Abandono_{tablename}')) %>% 
    as_tibble()
  
  f_desconectar_de_lezo()
  
  return(tablon)
}


write_tablon <- function(df, name, schema, tmp_dir ="\\\\wpg001cli\\lezor_shared\\temp\\"){
  f_ini_lezor()
  f_conecta_con_lezo()
  
  lezor::f_escribe_en_lezo(df = df,
                           nombre_tabla = glue("{schema}.Mod_Abandono_{name}"),
                           sobreescribir = F,
                           ruta_temp_files = tmp_dir)
  
  f_desconectar_de_lezo()
}


main <- function(opt){
  
  # Tablon data RDS format
  # test path
  tablon <- readRDS(opt$tablon)
  
  # Discretization configuration file
  categorias <- read_tsv(opt$config) # %>% 
  #filter(CLASS == 'TablonCPC')   # It would be eventually possible to filter rows by CLASS column: TablonCPC
  
  todays_date <- lubridate::today() %>% 
    as.character() %>% 
    str_replace_all('-', '')
  
  c_tablon <- categorias %>% 
    filter(Ramo == opt$ramo) %>% 
    group_by(Variable) %>% 
    nest() %>% 
    arrange(Variable)
  
  d_tablon <- apply(c_tablon, 1, discretize_variable, df=tablon %>% 
                      select(-Cliente_Codigo, -Poliza_Codigo, -A�oMes_Control , -Anula_Valor))
  
  names(d_tablon) <- c_tablon$Variable %>% as.character()
  
  d_tablon %<>% as_tibble()
  
  names(d_tablon) <- c_tablon$Variable %>% 
    as.character()
  
  d_tablon <- cbind(d_tablon, tablon[,c('Cliente_Codigo', 'Poliza_Codigo','A�oMes_Control', 'Anula_Valor')])
  
  d_tablon[is.na(d_tablon)] <- '99_No_Disponible'
  
  saveRDS(d_tablon, glue('{options$output}'))
  
  
}


option_list = list(
  make_option(c("-t", "--tablon"), 
              type = "character",
              default = "TablonAuto",
              help = "File with variables to include in RDS format.",
              metavar = "character"),
  make_option(c("-c", "--config"), 
              type = "character",
              default = " ",
              help = "Para discretizar el tablon.",
              metavar = "character"),
  make_option(c("-r", "--ramo"), 
              type = "character",
              default = "Auto",
              help = "Ramo_Valor: Auto | Hogar",
              metavar = "character"),
  make_option(c("-s", "--schema"), 
              type = "character",
              default = "PROY",
              help = "File with variables to include",
              metavar = "character"),
  make_option(c("-z", "--schemaOutput"), 
              type = "character",
              default = "PROY",
              help = "File with variables to include",
              metavar = "character"),
  make_option(c("-n", "--nas"), 
              type = "character",
              default = "99_No_Disponible",
              help = "Replacement for NAs.",
              metavar = "character"),
  make_option(c("-o", "--output"), 
              type = "character",
              default = NULL,
              help = "Path to store results",
              metavar = "character")
)

opt_parser <- OptionParser(option_list = option_list);
opt <- parse_args(opt_parser);

main(opt)