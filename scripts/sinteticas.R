# Sinteticas


set.seed(12345)
setwd('C:\\INMARK_Modelos_abandono\\rds')


library(lezor)
library(dplyr)
library(tibble)
library(lubridate)


execute_in_lezo <- function(query) {
    conexion <- .conexion_BLASDELEZO
    kk <- sqlQuery(conexion, query)
    if (length(kk) == 0) {
        cat('Ejecutada con exito\n')
    } else {
        cat(paste0(paste(kk), '\n'))
    }
}


fecha_control_to_date <- function(fecha, typo = 'first') {
    switch(typo,
           first = ymd(paste0(fecha, '01')),
           last = ymd(paste0(fecha, '01')) + months(1) - days(1)
           )
}


fecha_menos_periodo <- function(fecha, periodo) {
    # fecha en formato ymd
    fecha - years(periodo)
}

# ex: fecha_control_to_date('201710', 'first')

f_ini_lezor()
f_conecta_con_lezo()

clientes <- lezor::f_query_a_df(select = '*', from = 'WORK.Mod_Abandono_Lake_Clientes') %>% as_tibble()
saveRDS(clientes, 'clientes.rds')
rm(clientes)

polizas <- lezor::f_query_a_df(select = '*', from = 'WORK.Mod_Abandono_Lake_Polizas') %>% as_tibble()
saveRDS(polizas, 'polizas.rds')
rm(polizas)


#Mod_Abandono_Lake_GeoLocalizacion_Experian
experian <- lezor::f_query_a_df(select = '*', from = 'WORK.Mod_Abandono_Lake_GeoLocalizacion_Experian') %>% as_tibble()
saveRDS(experian, 'experian.rds')
rm(experian)

# Mod_Abandono_Lake_Proxima_Renovacion
proxima_renovacion <- lezor::f_query_a_df(select = '*', from = 'WORK.Mod_Abandono_Lake_Proxima_Renovacion') %>% as_tibble()
saveRDS(proxima_renovacion, 'proxima_renovacion.rds')
rm(proxima_renovacion)

# Mod_Abandono_Calculos_Competencia
competencia <- lezor::f_query_a_df(select = '*', from = 'WORK.Mod_Abandono_Calculos_Competencia') %>% as_tibble()
saveRDS(competencia, 'competencia.rds')
rm(competencia)

# Mod_Abandono_Lake_Proxima_Renovacion
productos <- lezor::f_query_a_df(select = '*', from = 'WORK.Mod_Abandono_Traduccion_Productos') %>% as_tibble()
saveRDS(productos, 'productos.rds')
rm(proxima_renovacion)

# Mod_Abandono_Lake_Riesgo_Auto
riesgo_auto <- lezor::f_query_a_df(select = '*', from = 'WORK.Mod_Abandono_Lake_Riesgo_Auto') %>% as_tibble()
saveRDS(riesgo_auto, 'riesgo_auto.rds')
rm(riesgo_auto)

# Mod_Abandono_Lake_Riesgo_Hogar
riesgo_hogar <- lezor::f_query_a_df(select = '*', from = 'WORK.Mod_Abandono_Lake_Riesgo_Hogar') %>% as_tibble()
saveRDS(riesgo_hogar, 'riesgo_hogar.rds')
rm(riesgo_hogar)


# Mod_Abandono_Lake_Polizas_Suplementos
polizas_suplementos <- lezor::f_query_a_df(select = '*', from = 'WORK.Mod_Abandono_Lake_Polizas_Suplementos') %>% as_tibble()
saveRDS(polizas_suplementos, 'polizas_suplementos.rds')
rm(polizas_suplementos)

# Mod_Abandono_Lake_PolizasHistorico
polizas_historico <- lezor::f_query_a_df(select = '*', from = 'WORK.Mod_Abandono_Lake_PolizasHistorico') %>% as_tibble()
saveRDS(polizas_historico, 'polizas_historico.rds')
rm(polizas_historico)

# Mod_Abandono_Lake_Recibos
recibos <- lezor::f_query_a_df(select = '*', from = 'WORK.Mod_Abandono_Lake_Recibos') %>% as_tibble()
saveRDS(recibos, 'recibos.rds')
rm(recibos)

# Mod_Abandono_Lake_Siniestros
siniestros <- lezor::f_query_a_df(select = '*', from = 'WORK.Mod_Abandono_Lake_Siniestros') %>% as_tibble()
saveRDS(siniestros, 'siniestros.rds')
rm(siniestros)

# Mod_Abandono_Lake_Mediadores
mediadores <- lezor::f_query_a_df(select = '*', from = 'WORK.Mod_Abandono_Lake_Mediadores') %>% as_tibble()
saveRDS(mediadores, 'mediadores.rds')
rm(mediadores)

# Mod_Abandono_Lake_ContactCenter
contact_center <- lezor::f_query_a_df(select = '*', from = 'WORK.Mod_Abandono_Lake_ContactCenter') %>% as_tibble()
saveRDS(contact_center, 'contact_center.rds')
rm(contact_center)

# Mod_Abandono_Lake_ContactCenter_Mediadores
contact_center_mediadores <- lezor::f_query_a_df(select = '*', from = 'WORK.Mod_Abandono_Lake_ContactCenter_Mediadores') %>% as_tibble()
saveRDS(contact_center_mediadores, 'contact_center_mediadores.rds')
rm(contact_center_mediadores)


# Mod_Abandono_Lake_Siniestros_Pagos
siniestros_pagos <- lezor::f_query_a_df(select = '*', from = 'WORK.Mod_Abandono_Lake_Siniestros_Pagos') %>% as_tibble()
saveRDS(siniestros_pagos, 'siniestros_pagos.rds')
rm(siniestros_pagos)


# Mod_Abandono_Lake_RentabilidadClientePoliza
rentabiliad_cliente_poliza <- lezor::f_query_a_df(select = '*', from = 'WORK.Mod_Abandono_Lake_RentabilidadClientePoliza') %>% as_tibble()
saveRDS(rentabiliad_cliente_poliza, 'rentabiliad_cliente_poliza.rds')
rm(rentabiliad_cliente_poliza)


# Mod_Abandono_Lake_Target
target <- lezor::f_query_a_df(select = '*', from = 'WORK.Mod_Abandono_Lake_Target') %>% as_tibble()
saveRDS(target, 'target.rds')
rm(target)


# Mod_Abandono_Lake_ClientesPeriodos
clientes_periodos <- lezor::f_query_a_df(select = '*', from = 'WORK.Mod_Abandono_Lake_ClientesPeriodos') %>% as_tibble()
saveRDS(clientes_periodos, 'clientes_periodos.rds')
rm(clientes_periodos)


# Mod_Abandono_Lake_Presupuestos
presupuestos <- lezor::f_query_a_df(select = '*', from = 'WORK.Mod_Abandono_Lake_Presupuestos') %>% as_tibble()
saveRDS(presupuestos, 'presupuestos.rds')
rm(presupuestos)

# Mod_Abandono_Lake_Calculos_Censo
calculos_censo <- lezor::f_query_a_df(select = '*', from = 'WORK.Mod_Abandono_Calculos_Censo') %>% as_tibble()
saveRDS(calculos_censo, 'calculos_censo.rds')
rm(calculos_censo)

proxima_renovacion <- lezor::f_query_a_df(select = '*', from = 'WORK.Mod_Abandono_Lake_Proxima_Renovacion') %>% as_tibble()
saveRDS(proxima_renovacion, 'proxima_renovacion.rds')
rm(proxima_renovacion)


sinteticas_clientes <- lezor::f_query_a_df(select = '*', from = 'WORK.Mod_Abandono_Sinteticas_Clientes') %>% as_tibble()
saveRDS(sinteticas_clientes, 'sinteticas_clientes.rds')
rm(sinteticas_clientes)

sinteticas_clientes <- lezor::f_query_a_df(select = '*', from = 'WORK.Mod_Abandono_Sinteticas_Clientes') %>% as_tibble()
saveRDS(sinteticas_clientes, 'sinteticas_clientes.rds')
rm(sinteticas_clientes)


tablon_auto <- lezor::f_query_a_df(select = '*', from = 'WORK.Mod_Abandono_TablonAuto') %>% as_tibble()
saveRDS(tablon_auto, 'tablon_auto.rds')
rm(sinteticas_clientes)


tablon_hogar <- lezor::f_query_a_df(select = '*', from = 'WORK.Mod_Abandono_TablonHogar') %>% as_tibble()
saveRDS(tablon_hogar, 'tablon_hogar.rds')
rm(sinteticas_hogar)


proxima_renovacion_proy <- lezor::f_query_a_df(select = '*', from = 'PROY.Mod_Abandono_Lake_Proxima_Renovacion') %>% as_tibble()
saveRDS(proxima_renovacion_proy, 'proxima_renovacion_proy.rds')
rm(proxima_renovacion_proy)


f_desconectar_de_lezo()


polizas %>%
    group_by(Poliza_Codigo, VigenciaInicial_Fecha, VigenciaFinal_Fecha) %>%
    summarise(cuenta = n(), ramos_distinto = unique(Ramo_Valor)) %>%
    arrange(cuenta, Poliza_Codigo, VigenciaInicial_Fecha, VigenciaFinal_Fecha) %>%
    head()

polizas_historico <- readRDS('polizas_historico.rds')
j <- inner_join(polizas, polizas_historico, by = 'Cliente_Codigo')

# Query 1
# sus_date <- function(x, y) date(x) - year(y)
# fechas <- list(perido0=function(x) date(x) - year(1), periodo1=function(x) data - year(2), periodo2=function(x) date(x) -year(1))
query1 <- function(polizas, polizas_historico, periodo) {
    inner_join(polizas, polizas_historico, by = 'Cliente_Codigo') %>%
        filter(Cliente_Codigo == 1558042) %>%
        group_by(Cliente_Codigo, Poliza_Codigo.x, A�oMes_Control) %>%
        mutate(date = switch(periodo, cero = A�oMes_Control,
                                        menos_uno = VigenciaFinal_Fecha.x,
                                        menos_dos = VigenciaFinal_Fecha.x)) %>%
    # filter((VigenciaInicial_Fecha.y <= A�oMes_Control) & (VigenciaVencimiento_Fecha.y >= VigenciaFinal_Fecha.x)) %>%
    filter((VigenciaInicial_Fecha.y <= date) & (VigenciaVencimiento_Fecha.y >= VigenciaFinal_Fecha.x)) %>%
    # select(Cliente_Codigo, Poliza_Codigo.x, date, VigenciaInicial_Fecha.y, VigenciaFinal_Fecha.x) %>%
    group_by(Poliza_Codigo.x) %>%
        summarise(n = length(unique(Ramo_Valor.y))) %>%
        select(Poliza_Codigo.x, n)
    select(Cliente_Codigo, Poliza_Codigo.x, A�oMes_Control, ultima, VigenciaInicial_Fecha.y, VigenciaFinal_Fecha.x)
}

# Diferencias en SinteticasClientes
#
# SinteticasClientes$diff1 <- SinteticasClientes$periodo0 - SinteticasClientes$periodoMenos1
# SinteticasClientes$diff2 <- SinteticasClientes$periodoMenos1 - SinteticasClientes$periodoMenos2



# Polizas Saneadas
# guardar_join(polizas,polizas_historico)
# Mod_Abandono_Traduccion_Anulacion_Motivos
polizas_saneadas <- function(polizas, polizas_historico) {
    inner_join(polizas, polizas_historico, by = 'Cliente_Codigo') %>%
    mutate(a_mes_control = fecha_control_to_date(A�oMes_Control, 'first'),
           anos_menos3 = fecha_control_to_date(A�oMes_Control, 'first', 3)) %>%
    filter(SuplementoTipo_Codigo == 'AT' &
           (a_mes_control >= VigenciaVencimiento_Fecha.y) &
           (VigenciaVencimiento_Fecha.y < anos_menos3) &
           (MotivoAnulacion_Codigo.y %in% c(9, 13, 14, 17, 21, 23, 51, 55, 74))) %>%
    group_by(Cliente_Codigo, Poliza_Codigo.x, a_mes_control) %>%
    summarise(n = n())
}

res <- polizas_saneadas(polizas, polizas_historico)


polizas <- readRDS('polizas.rds')
polizas_historico <- readRDS('polizas_historico.rds')

# Explicacion para estos casos: 
polizas_actuales <- function(polizas, polizas_historico, tipo = NULL) {
    # typo puede ser auto o bien hogar
    # A�oMes_Control <- transformar ultimpo dia mes actual dado
    f <- inner_join(polizas, polizas_historico, by = 'Cliente_Codigo') %>%
        filter(Cliente_Codigo == 1558042) %>%
        mutate(a_mes_control = fecha_control_to_date(A�oMes_Control, 'first')) %>%
        group_by(Cliente_Codigo, Poliza_Codigo.x, A�oMes_Control) %>%
        ungroup() %>%
    filter((VigenciaInicial_Fecha.y <= a_mes_control) &
    (VigenciaVencimiento_Fecha.y > a_mes_control)) %>%
    group_by(Cliente_Codigo, Poliza_Codigo.y, a_mes_control) %>%
        summarise(n = n()) %>%
        arrange(desc(n)) %>%
    select(Cliente_Codigo, Poliza_Codigo.x, n) %>%
    unique()
}

res <- polizas_actuales(polizas, polizas_historico)
res

# PolizasNuevasDesdeRenovacion_Entero
# Explicacion para estos casos:
# VigenciaInicial_Fecha.y <= a_mes_control :: Limite inferior
# VigenciaInicial_Fecha.y >= VigenciaFinal_Fecha.x :: Limite superior
polizas_nuevas_desde_renovacion <- function(polizas, polizas_historico) {
    inner_join(polizas, polizas_historico, by = 'Cliente_Codigo') %>%
    mutate(a_mes_control = fecha_control_to_date(A�oMes_Control, 'last')) %>%
    filter(Cliente_Codigo == 1558042) %>%
    filter(VigenciaInicial_Fecha.y <= a_mes_control & VigenciaInicial_Fecha.y >= VigenciaFinal_Fecha.x) %>%
    group_by(Cliente_Codigo, Poliza_Codigo.x, Poliza_Codigo.y, A�oMes_Control) %>%
    summarise(n = n()) %>%
    select(Cliente_Codigo, Poliza_Codigo.x, A�oMes_Control) %>%
    arrange(A�oMes_Control)
}

res <- polizas_nuevas_desde_renovacion(polizas, polizas_historico)
res

# PolizasNuevasAutoDesdeRenovacion_Entero
# PolizasNuevas_Entero_PeriodoMenos01
# PolizasNuevasAuto_Entero_PeriodoMenos01
# PolizasNuevasHogar_Entero_PeriodoMenos01
# PolizasNuevas_Entero_PeriodoMenos02
# PolizasNuevasAuto_Entero_PeriodoMenos02
# PolizasNuevasHogar_Entero_PeriodoMenos02
# Poliza_Codigo.x => puede ser de cualquier tipo 
## !!! HISTORICO ANALIZADO HASTA HOY (FECHA ACTUAL)
# Vigencicia Inicial -> creacion
# Vigencia Final -> renovacion ultima para periodo actual
# Vigencia Vencimiento -> cuando tengo que hacer renov
# A�oMes control -> fecha de referencia
polizas_nuevas_desde_renovacion_tipo <- function(polizas, polizas_historico, periodo = 0) {
    periodo <- years(periodo)
    f <- inner_join(polizas, polizas_historico, by = 'Cliente_Codigo') %>%
    mutate(a_mes_control = fecha_control_to_date(A�oMes_Control, 'last'))
    #filter(Cliente_Codigo == 1558042) %>%
    # filter(Ramo_Valor.y %in% c('AUTOMOVILES', 'HOGAR'))
    if (periodo == 0) {
        f <- f %>%
        filter((VigenciaInicial_Fecha.y <= a_mes_control) & (VigenciaInicial_Fecha.y >= VigenciaFinal_Fecha.x))
    } else {
        f <- f %>% filter((VigenciaInicial_Fecha.y >= (ymd(VigenciaFinal_Fecha.x) - periodo)) & (VigenciaInicial_Fecha.y <= (ymd(VigenciaVencimiento_Fecha.x) - periodo)))
    }
    f %>% group_by(Cliente_Codigo, Poliza_Codigo.x, Poliza_Codigo.y, a_mes_control, Ramo_Valor.y) %>%
    summarise(n = n()) %>%
    ungroup() %>%
    unique()
}

res <- polizas_nuevas_desde_renovacion_tipo(polizas, polizas_historico)
res1 <- polizas_nuevas_desde_renovacion_tipo(polizas, polizas_historico, 1)
res2 <- polizas_nuevas_desde_renovacion_tipo(polizas, polizas_historico, 2)


# PolizasAnuladasTotal_Entero
# PolizasAnuladasAutoTotal_Entero
# PolizasAnuladasHogarTotal_Entero
# PolizasAnuladasTotal_Entero_PeriodoMenos01
# PolizasAnuladasAutoTotal_Entero_PeriodoMenos01
# PolizasAnuladasHogarTotal_Entero_PeriodoMenos01
# PolizasAnuladasTotal_Entero_PeriodoMenos02
# PolizasAnuladasAutoTotal_Entero_PeriodoMenos2
# PolizasAnuladasHogarTotal_Entero_PeriodoMenos02

#? Calcular polizas anuladas para un periodo X:
# x -> polizas que no interesa para el analisis
polizas_anuladas_tipo_periodo <- function(polizas, polizas_historico, periodo = 0) {
    periodo <- years(periodo)
    f <- inner_join(polizas, polizas_historico, by = 'Cliente_Codigo') %>%
    mutate(a_mes_control = fecha_control_to_date(A�oMes_Control, 'first')) %>%
    filter(SuplementoTipo_Codigo == 'AT')
    if (periodo == 0) {
        f <- f %>%
        filter((VigenciaInicial_Fecha.y <= a_mes_control) & (VigenciaVencimiento_Fecha.y >= VigenciaFinal_Fecha.x))
    } else {
        f <- f %>% filter((VigenciaVencimiento_Fecha.y >= (ymd(VigenciaFinal_Fecha.x) - periodo)) & (VigenciaVencimiento_Fecha.y <= (ymd(VigenciaVencimiento_Fecha.x) - periodo)))
    }
    f %>% group_by(Cliente_Codigo, Poliza_Codigo.x, Poliza_Codigo.y, a_mes_control, Ramo_Valor.y) %>%
    summarise(n = n()) %>%
    ungroup() %>%
    unique()
}

res <- polizas_anuladas_tipo_periodo(polizas, polizas_historico)


experian <- readRDS('experian.rds')
## CLASIFICADAS COMO MANUALES
#DireccionNueva_Valor -> Valor no nulo para experian periodo 0
# Proceso_Fecha: fecha en el que se modifico tabla experian
nueva_direccion <- function(polizas, experian) {
    inner_join(polizas, experian, by = 'Cliente_Codigo') %>%
    mutate(a_mes_control = fecha_control_to_date(A�oMes_Control, 'last')) %>%
    filter(Proceso_Fecha <= a_mes_control & Proceso_Fecha >= VigenciaFinal_Fecha) %>%
    group_by(Cliente_Codigo, Poliza_Codigo, a_mes_control) %>%
    summarise(n = n()) %>%
    ungroup() %>%
    unique()
}

nd <- nueva_direccion(polizas, experian)

#ClienteGuadiana_Valor

clientes_periodos <- lezor::f_query_a_df(select = '*', from = 'WORK.Mod_Abandono_Lake_ClientesPeriodos') %>% as_tibble()
saveRDS(clientes_periodos, 'clientes_periodos.rds')
# rm(clientes_periodos)

# el cliente en la tabla Mod_Abandono_Lake_ClientesPeriodos, tenga en el campo[Vigencia_Entero], 
# mas de2 valores distintos, los impares son para periodos que esta de alta, 
# los pares que no esta de alta, osea si tiene 3 periodos o mas es que estaba, se fue y luego volvio.
cliente_guadiana <- function(polizas, clientes_periodos) {
    inner_join(polizas, clientes_periodos, by = 'Cliente_Codigo') %>%
    mutate(a_mes_control = fecha_control_to_date(A�oMes_Control, 'last')) %>%
    filter(Vigencia_Entero >= 3) %>%
    select(Cliente_Codigo, Poliza_Codigo, a_mes_control, Vigencia_Entero) 
}
res <- cliente_guadiana(polizas, clientes_periodos)

#Compa�iaProcedencia_Valor -> NO SE HACE


#CambioMediadorActual_Valor
polizas_suplementos <- readRDS('polizas_suplementos.rds')

cambio_mediador <- function(polizas, polizas_suplementos) {
    inner_join(polizas, polizas_suplementos, by = 'Cliente_Codigo') %>%
    mutate(a_mes_control_y = fecha_control_to_date(A�oMes_Control.y, 'last'),
    a_mes_control_x = fecha_control_to_date(A�oMes_Control.x, 'last')) %>%
    filter((a_mes_control_y <= a_mes_control_x) & (a_mes_control_y >= VigenciaFinal_Fecha.x)) %>%
    filter(MediadorActual_Codigo.x != MediadorActual_Codigo.y) %>%
    select(Cliente_Codigo, Poliza_Codigo.y, a_mes_control_x) %>%
    mutate(CambioMediadorActual_Valor = 1)
}

res <- cambio_mediador(polizas, polizas_suplementos)


# PolizasRenovadas_Entero_Periodo0 ->
polizas <- readRDS('polizas.rds') %>% as_tibble()
polizas_historico <- readRDS('polizas_historico.rds') %>% as_tibble()
polizas_suplementos <- readRDS('polizas_suplementos.rds') %>% as_tibble()
proxima_renovacion <- readRDS('proxima_renovacion.rds') %>% as_tibble()

# Tomamos las polizas cuyo periodo de renovacion (mes) cae dentro del periodo estimado.
polizas_renovadas_periodo0 <- function(polizas_historico, proxima_renovacion, periodo = 0) {
    res <- inner_join(polizas_historico, proxima_renovacion, by = 'Poliza_Codigo') %>%
    filter(Renovacion_Fecha <= ymd('2017-09-30') & Renovacion_Fecha > ymd('2016-12-31')) %>%
    group_by(Cliente_Codigo) %>% summarise(n=n())
}


# Entendemos ultimos y proximos X meses desde FechaaVencimientoVigencia de la poliza.
# PolizasRenovadas_Entero_Ultimos5M -> periodo de vigencia de la poliza.
polizas_renovadas_periodo5m <- function(polizas_historico, proxima_renovacion, periodo = 0) {

}

# PolizasARenovar_Entero_Proximos3M
polizas_renovadas_periodo3m <- function(polizas_historico, proxima_renovacion, periodo = 0) {
    res <- inner_join(polizas_historico, proxima_renovacion, by = 'Poliza_Codigo') %>%
    filter(A�oMes_Control <= ymd('2017-09-30') & Renovacion_Fecha > ymd('2016-12-31')) %>%
    group_by(Cliente_Codigo) %>% summarise(n = n())
}


# SiniestrosReaperturas_Entero

# LlamadasObtenerCotizacionesPeriodoActual_Entero
# LlamadasObtenerConsultaReciboPeriodoActual_Entero
# LlamadasObtenerCotizarDowngradesPeriodoActual_Entero


# LlamadasTotales_Entero_Ultimos5M
# PresupuestosPedidos_Entero
# PresupuestosPedidosAuto_Entero
# PresupuestosPedidosHogar_Entero


# SiniestrosTotales_Entero_Periodo0
# SiniestrosAutos_Entero_Periodo0
# SiniestrosDiversos_Entero_Periodo0
# SiniestrosTotales_Entero_Ultimos06M
# SiniestrosTotales_Entero_Ultimos36M
