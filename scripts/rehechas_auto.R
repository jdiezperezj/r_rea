library(dplyr)
library(tibble)
library(lubridate)
library(stringr)
library(readr)
library(magrittr)
library(tidyr)
library(smbinning)
library(Hmisc)
library(xlsx)

monobin <- function(data, y, x) {
  d1 <- data[c(y, x)]
  n <- min(20, nrow(unique(d1[x])))
  repeat {
    d1$bin <- Hmisc::cut2(d1[, x], g = n)
    d2 <- aggregate(d1[-3], d1[3], mean)
    c <- cor(d2[-1], method = "spearman") 
    if (abs(c[1, 2])==1 | n == 2) break
    n <- n - 1
  }
  d3 <- aggregate(d1[-3], d1[3], max)
  cuts <- d3[-length(d3[, 3]), 3]
  res <- smbinning::smbinning.custom(d1, y, x, cuts)
  print(head(res))
  return(res)
}

setwd('C:\\INMARK_Modelos_abandono\\rds')

target <- readRDS('target.rds') %>% 
  as_tibble() %>% 
  filter(A�oMes_Control < 201707 & Ramo_Valor %in% c('AUTOMOVILES','HOGAR'))

SiniestrosTotales_Entero_Periodo0 <- readRDS('SiniestrosTotales_Entero_Periodo0.rds')
SiniestrosAutos_Entero_Periodo0 <- readRDS('SiniestrosAutos_Entero_Periodo0.rds')
SiniestrosDiversos_Entero_Periodo0 <- readRDS('SiniestrosDiversos_Entero_Periodo0.rds')
SiniestrosTotales_Entero_Ultimos06M <- readRDS('SiniestrosTotales_Entero_Ultimos06M.rds')
SiniestrosTotales_Entero_Ultimos36M <- readRDS('SiniestrosTotales_Entero_Ultimos36M.rds')
CambioPeriodicidadaPagoFraccionado_Valor <- readRDS('CambioPeriodicidadaPagoFraccionado_Valor.rds')
CambioPeriodicidadaPagoUnico_Valor <- readRDS('CambioPeriodicidadaPagoUnico_Valor.rds') 
AgregaPolizaAdicionalAuto_Valor <- readRDS('AgregaPolizaAdicionalAuto_Valor.rds')
AgregaPolizaAdicionalHogar_Valor <- readRDS('AgregaPolizaAdicionalHogar_Valor.rds')
PolizasARenovar_Entero_Proximos3M <- readRDS('PolizasARenovar_Entero_Proximos3M.rds')
ReaperturasSiniestros_Entero <- readRDS('ReaperturasSiniestros_Entero.rds')
Rehuses_Entero <- readRDS('Rehuses_Entero.rds')
TieneAlgunaPolizaEnSINCO_Valor <- readRDS('TieneAlgunaPolizaEnSINCO_Valor.rds')

resultado <- left_join(target, SiniestrosTotales_Entero_Periodo0, by='Poliza_Codigo') %>% 
  select(-A�oMes_Control.y, -Cliente_Codigo.y, -MesAnulacion_Entero, -MotivoAnulacion_Codigo) %>%
  dplyr::rename(Cliente_Codigo=Cliente_Codigo.x, A�oMes_Control=A�oMes_Control.x)

resultado <- left_join(resultado, SiniestrosAutos_Entero_Periodo0, by='Poliza_Codigo') %>% 
  select(-A�oMes_Control.y, -Cliente_Codigo.y) %>% 
  dplyr::rename(Cliente_Codigo=Cliente_Codigo.x, A�oMes_Control=A�oMes_Control.x)

resultado <- left_join(resultado, SiniestrosDiversos_Entero_Periodo0, by='Poliza_Codigo') %>% 
  select(-A�oMes_Control.y, -Cliente_Codigo.y) %>% 
  dplyr::rename(Cliente_Codigo=Cliente_Codigo.x, A�oMes_Control=A�oMes_Control.x)

resultado <- left_join(resultado, SiniestrosTotales_Entero_Ultimos06M, by='Poliza_Codigo') %>% 
  select(-A�oMes_Control.y, -Cliente_Codigo.y) %>% 
  dplyr::rename(Cliente_Codigo=Cliente_Codigo.x, A�oMes_Control=A�oMes_Control.x)

resultado <- left_join(resultado, SiniestrosTotales_Entero_Ultimos36M, by='Poliza_Codigo') %>% 
  select(-A�oMes_Control.y, -Cliente_Codigo.y) %>% 
  dplyr::rename(Cliente_Codigo=Cliente_Codigo.x, A�oMes_Control=A�oMes_Control.x)

resultado <- left_join(resultado, CambioPeriodicidadaPagoFraccionado_Valor, by='Poliza_Codigo') %>% 
  select(-A�oMes_Control.y, -Cliente_Codigo.y) %>% 
  dplyr::rename(Cliente_Codigo=Cliente_Codigo.x, A�oMes_Control=A�oMes_Control.x)

resultado <- left_join(resultado, CambioPeriodicidadaPagoUnico_Valor, by='Poliza_Codigo') %>% 
  select(-A�oMes_Control.y, -Cliente_Codigo.y) %>% 
  dplyr::rename(Cliente_Codigo=Cliente_Codigo.x, A�oMes_Control=A�oMes_Control.x)

resultado <- left_join(resultado, AgregaPolizaAdicionalAuto_Valor, by='Poliza_Codigo') %>% 
  select(-A�oMes_Control.y, -Cliente_Codigo.y) %>% 
  dplyr::rename(Cliente_Codigo=Cliente_Codigo.x, A�oMes_Control=A�oMes_Control.x)

resultado <- left_join(resultado, AgregaPolizaAdicionalHogar_Valor, by='Poliza_Codigo') %>% 
  select(-A�oMes_Control.y, -Cliente_Codigo.y) %>% 
  dplyr::rename(Cliente_Codigo=Cliente_Codigo.x, A�oMes_Control=A�oMes_Control.x)

resultado <- left_join(resultado, PolizasARenovar_Entero_Proximos3M, by='Poliza_Codigo') %>% 
  select(-A�oMes_Control.y, -Cliente_Codigo.y) %>% 
  dplyr::rename(Cliente_Codigo=Cliente_Codigo.x, A�oMes_Control=A�oMes_Control.x)

resultado <- left_join(resultado, ReaperturasSiniestros_Entero, by='Poliza_Codigo') %>% 
  select(-A�oMes_Control.y, -Cliente_Codigo.y) %>% 
  dplyr::rename(Cliente_Codigo=Cliente_Codigo.x, A�oMes_Control=A�oMes_Control.x)

resultado <- left_join(resultado, Rehuses_Entero, by='Poliza_Codigo') %>% 
  select(-A�oMes_Control.y, -Cliente_Codigo.y) %>% 
  dplyr::rename(Cliente_Codigo=Cliente_Codigo.x, A�oMes_Control=A�oMes_Control.x)

resultado <- left_join(resultado, TieneAlgunaPolizaEnSINCO_Valor, by='Poliza_Codigo') %>% 
  select(-A�oMes_Control.y, -Cliente_Codigo.y) %>% 
  dplyr::rename(Cliente_Codigo=Cliente_Codigo.x, A�oMes_Control=A�oMes_Control.x)

resultado %>% select(-Cliente_Codigo, -Poliza_Codigo, -Anula_Valor, -A�oMes_Control,-Ramo_Valor) %>% summary()

ns <- resultado %>% select(-Cliente_Codigo, -Poliza_Codigo, -Anula_Valor, -A�oMes_Control,-Ramo_Valor) %>% 
  colnames()


fs <- function(name, data){
  res <- tryCatch(
    {w <- monobin(data, 'Anula_Valor', name)
    if (typeof(w) == 'list') {
      df <- w$ivtable
      df$Consulta <- name
      df$Variable <- 'Sinteticas'
      return(df)
    }else{
      return(NULL)
    }
    },
    error=function(e){return(NULL)}
  )
  return(res)
}


resultado_auto <- resultado %>% filter(Ramo_Valor == 'AUTOMOVILES')
saveRDS(resultado_auto, "sinteticas_rehechas_auto.rds")
resultado_hogar <- resultado %>% filter(Ramo_Valor == 'HOGAR')
saveRDS(resultado_hogar, "sinteticas_rehechas_hogar.rds")

resultado_auto %>% select(-Cliente_Codigo, -Poliza_Codigo, -Anula_Valor, -A�oMes_Control,-Ramo_Valor) %>% summary()
resultado_hogar %>% select(-Cliente_Codigo, -Poliza_Codigo, -Anula_Valor, -A�oMes_Control,-Ramo_Valor) %>% summary()


da <- lapply(ns, fs, data=resultado_auto %>% as.data.frame())
res_auto <- do.call(rbind, da)


dh <- lapply(ns, fs, data=resultado_hogar %>% as.data.frame())
res_hogar <- do.call(rbind, dh)


xlsx::write.xlsx(res_auto, 'sinteticas_rechechas_auto.xlsx')
xlsx::write.xlsx(res_hogar, 'sinteticas_rechechas_hogar.xlsx')
